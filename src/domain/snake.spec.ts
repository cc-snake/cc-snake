import { SnakeState, tickSnake, Size, turnRight } from './snake';

describe('snake', function() {
  const commonSnakeState: SnakeState = {
    position: {
      x: 0,
      y: 0,
    },
    direction: {
      x: 1,
      y: 0,
    },
  };

  const boundaries: Size = {
    x: 10,
    y: 10,
  };

  describe('tick', function() {
    it('should return snake state of next tick', () => {
      const nextState = tickSnake(commonSnakeState, boundaries);
      expect(nextState).toEqual({
        ...commonSnakeState,
        position: {
          x: 1,
          y: 0,
        },
      });
    });

    it('should move snake to left side when moving above right edge', function() {
      const nextState = tickSnake(
        {
          ...commonSnakeState,
          position: {
            x: 9,
            y: 0,
          },
        },
        boundaries,
      );
      expect(nextState).toEqual({
        ...commonSnakeState,
        position: {
          x: 0,
          y: 0,
        },
      });
    });

    it('should move snake down', () => {
      const currentState: SnakeState = {
        position: {
          x: 0,
          y: 0,
        },
        direction: {
          x: 0,
          y: 1,
        },
      };
      const nextState = tickSnake(currentState, boundaries);
      expect(nextState).toEqual({
        ...currentState,
        position: {
          x: 0,
          y: 1,
        },
      });
    });

    it('should move snake to top when it reaches the bottom', function() {
      const currentState: SnakeState = {
        direction: {
          x: 0,
          y: 1,
        },
        position: {
          x: 0,
          y: 9,
        },
      };
      const nextState = tickSnake(currentState, boundaries);
      expect(nextState).toEqual({
        ...currentState,
        position: {
          x: 0,
          y: 0,
        },
      });
    });

    it('should move snake to right when it reaches the left edge', function() {
      const currentState: SnakeState = {
        direction: {
          x: -1,
          y: 0,
        },
        position: {
          x: 0,
          y: 0,
        },
      };
      const nextState = tickSnake(currentState, boundaries);
      expect(nextState).toEqual({
        ...currentState,
        position: {
          x: 9,
          y: 0,
        },
      });
    });

    it('should move snake to bottom when it reaches the top edge', function() {
      const currentState: SnakeState = {
        direction: {
          x: 0,
          y: -1,
        },
        position: {
          x: 0,
          y: 0,
        },
      };
      const nextState = tickSnake(currentState, boundaries);
      expect(nextState).toEqual({
        ...currentState,
        position: {
          x: 0,
          y: 9,
        },
      });
    });
  });

  describe('turns', () => {
    test.each([
      { initial: { x: 0, y: 1 }, expected: { x: -1, y: 0 } },
      { initial: { x: -1, y: 0 }, expected: { x: 0, y: -1 } },
      { initial: { x: 0, y: -1 }, expected: { x: 1, y: 0 } },
      { initial: { x: 1, y: 0 }, expected: { x: 0, y: 1 } },
    ])('should change direction when turning right', function({
      initial,
      expected,
    }) {
      const snakeState = {
        ...commonSnakeState,
        direction: initial,
      };
      const nextState = turnRight(snakeState);

      expect(nextState.direction).toEqual(expected);
    });
  });
});
