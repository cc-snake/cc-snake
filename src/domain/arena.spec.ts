import { ArenaState, tickArena } from './arena';

const commonArenaState: ArenaState = {
  size: {
    x: 10,
    y: 10,
  },
  snake: {
    position: {
      x: 0,
      y: 0,
    },
    direction: {
      x: 0,
      y: 0,
    },
  },
};

describe('tickArena', () => {
  it('should return arena state of next tick', () => {
    const arenaState = tickArena(commonArenaState);
    expect(arenaState).toEqual({
      ...commonArenaState,
    });
  });
});
