export type SnakeState = {
  position: Position;
  direction: Direction;
};

type Vector2D = {
  x: number;
  y: number;
};

export type Position = Vector2D;
export type Direction = Vector2D;
export type Size = Vector2D;

export function tickSnake(snake: SnakeState, boundaries: Size): SnakeState {
  return {
    ...snake,
    position: {
      x: (snake.position.x + snake.direction.x + boundaries.x) % boundaries.x,
      y: (snake.position.y + snake.direction.y + boundaries.y) % boundaries.y,
    },
  };
}

export function turnRight({
  direction,
  ...snakeState
}: SnakeState): SnakeState {
  return {
    ...snakeState,
    direction: {
      x: direction.x === 0 ? -1 * direction.y : 0,
      y: direction.y === 0 ? 1 * direction.x : 0,
    },
  };
}
