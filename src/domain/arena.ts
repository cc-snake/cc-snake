import { SnakeState, tickSnake, Size } from './snake';

export function tickArena(arenaState: ArenaState): ArenaState {
  return {
    ...arenaState,
    snake: tickSnake(arenaState.snake, arenaState.size),
  };
}

export type ArenaState = {
  size: Size;
  snake: SnakeState;
};

export default function arenaBuilder(): ArenaState {
  return {
    size: {
      x: 10,
      y: 10,
    },
    snake: {
      position: {
        x: 0,
        y: 0,
      },
      direction: {
        x: 1,
        y: 0,
      },
    },
  };
}
