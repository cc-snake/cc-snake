import React from 'react';
import './App.css';
import Arena from './Arena';
import arenaBuilder from '../domain/arena';
import { useArena } from './useArena';

const initialState = arenaBuilder();

const App: React.FC = () => {
  const { arenaState, turnRight } = useArena(initialState);
  return <Arena state={arenaState} turnRight={turnRight} />;
};

export default App;
