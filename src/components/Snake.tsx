import React from 'react';
import { worldToScreen } from './Arena';
import { Position } from '../domain/snake';

type SnakeProps = {
  position: Position;
};

export default function Snake({ position }: SnakeProps) {
  return (
    <div
      style={{
        left: worldToScreen(position.x),
        top: worldToScreen(position.y),
        width: worldToScreen(1),
        height: worldToScreen(1),
        position: 'absolute',
        backgroundColor: 'hotpink',
      }}
    />
  );
}
