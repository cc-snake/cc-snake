import { number } from 'prop-types';
import { worldToScreen } from './Arena';

describe('screen scaling', function() {
  it('should scale scalar to screen size', function() {
    expect(worldToScreen(10)).toBe(700);
  });
});
