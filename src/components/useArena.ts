import React from 'react';
import { ArenaState, tickArena } from '../domain/arena';
import { turnRight as turnSnakeRight } from '../domain/snake';

export function useArena(initialState: ArenaState) {
  const [arenaState, setArenaState] = React.useState(initialState);

  React.useEffect(() => {
    const id = setInterval(() => {
      setArenaState(tickArena);
    }, 300);
    return () => clearInterval(id);
  }, []);

  const turnRight = () => {
    const nextSnakeState = turnSnakeRight(arenaState.snake);
    setArenaState({
      ...arenaState,
      snake: nextSnakeState,
    });
  };

  return {
    arenaState,
    turnRight,
  };
}
