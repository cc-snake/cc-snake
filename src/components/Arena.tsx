import React from 'react';
import Snake from './Snake';
import { ArenaState } from '../domain/arena';

type ArenaProps = {
  state: ArenaState;
  turnRight: () => void;
};

export const unitSize = 70;

export function worldToScreen(gameSize: number) {
  return gameSize * unitSize;
}

export default function Arena(props: ArenaProps) {
  return (
    <div
      style={{
        width: worldToScreen(props.state.size.x),
        height: worldToScreen(props.state.size.y),
        backgroundColor: 'ghostwhite',
        position: 'absolute',
      }}
      onClick={evt => {
        props.turnRight();
        evt.preventDefault();
      }}
    >
      <Snake {...props.state.snake} />
    </div>
  );
}
